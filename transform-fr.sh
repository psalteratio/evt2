#!/bin/bash

# 変換処理を行う関数
transform() {
  input=$1
  output=$2

  temp=temp1.xml
  echo "Starting transformation for $input"  # ログ出力
  java -jar lib/SaxonHE12-5J/saxon-he-12.5.jar -s:"$input" -xsl:xslt/1-pbcb-gap.xsl -o:"$temp"
  
  for i in 2 3 4 5 6 7 8; do
    next_temp="temp${i}.xml"
    case $i in
      2) xsl_file="xslt/2-main.xsl" ;;
      3) xsl_file="xslt/3-open-l-add-supplied.xsl" ;;
      4) xsl_file="xslt/4-supr-hors-lg.xsl" ;;
      5) xsl_file="xslt/5-lb.xsl" ;;
      6) xsl_file="xslt/6-lb-to-l.xsl" ;;
      7) xsl_file="xslt/7-double-l-sup.xsl" ;;
      8) xsl_file="xslt/8-del-endespace.xsl" ;;
    esac
    echo "Applying XSLT $xsl_file to $temp"  # ログ出力
    java -jar lib/SaxonHE12-5J/saxon-he-12.5.jar -s:"$temp" -xsl:"$xsl_file" -o:"$next_temp"
    temp="$next_temp"
  done
  echo "Final output saved to $output"  # ログ出力
  mv "$temp" "$output"
}

# 入力ディレクトリと出力ディレクトリ
input_dir="2-teinte"
output_dir="3-witness"

# 出力ディレクトリが存在しなければ作成
mkdir -p "$output_dir"
echo "Processing files from $input_dir to $output_dir"  # ログ出力

# 指定ディレクトリ内のXMLファイルを処理
for input_file in "$input_dir"/*.xml; do
  # 出力ファイル名を決定
  base_name=$(basename "$input_file" .xml)
  output_file="$output_dir/${base_name}.xml"

  # 変換処理を実行
  echo "Transforming $input_file to $output_file"  # ログ出力
  transform "$input_file" "$output_file"
done

echo "All transformations completed."  # ログ出力

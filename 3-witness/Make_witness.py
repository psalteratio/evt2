from lxml import etree
from collections import defaultdict
import re
from copy import deepcopy

def parse_lg_number(lg_number):
    match = re.match(r'Ps(\d+)-([a-z]*)(\d+)((bis|[a-z]*)?-?[0-9]?)', lg_number)
    if match:
        pnumber = int(match.group(1))
        prefix = match.group(2)
        verse = int(match.group(3))
        suffix = match.group(4)
        return (pnumber, prefix, verse, suffix)
    return (float('inf'), "", float('inf'), "")

def process_xml_with_header(files):
    grouped_psalms = defaultdict(list)
    namespaces = {'tei': 'http://www.tei-c.org/ns/1.0', 'xml': 'http://www.w3.org/XML/1998/namespace'}

    for file in files:
        parser = etree.XMLParser(recover=True)
        try:
            tree = etree.parse(file, parser)
            root = tree.getroot()
        except etree.XMLSyntaxError as e:
            print(f"Error parsing {file}: {e}")
            continue

        lg_elements = root.xpath(".//tei:lg", namespaces=namespaces)
        for lg_element in lg_elements:
            lg_number = lg_element.attrib.get("n")
            if not lg_number:
                continue

            pnumber, prefix, verse, suffix = parse_lg_number(lg_number)
            grouped_psalms[(pnumber, prefix, verse, suffix)].append((file, lg_element))

    new_root = etree.Element("{http://www.tei-c.org/ns/1.0}TEI", nsmap={None: "http://www.tei-c.org/ns/1.0"})
    tei_header = etree.SubElement(new_root, "teiHeader")
    file_desc = etree.SubElement(tei_header, "fileDesc")
    
    title_stmt = etree.SubElement(file_desc, "titleStmt")
    title = etree.SubElement(title_stmt, "title")
    title.text = "Title"

    publication_stmt = etree.SubElement(file_desc, "publicationStmt")
    publication_info = etree.SubElement(publication_stmt, "p")
    publication_info.text = "Publication Information"

    source_desc = etree.SubElement(file_desc, "sourceDesc")
    list_wit = etree.SubElement(source_desc, "listWit")

    xml_ns = "http://www.w3.org/XML/1998/namespace"
    for file in files:
        file_id = file.split('.')[0]
        witness = etree.SubElement(list_wit, "witness")
        witness.set(f"{{{xml_ns}}}id", file_id)

    text = etree.SubElement(new_root, "text")
    body = etree.SubElement(text, "body")

    sorted_keys = sorted(grouped_psalms.keys())
    current_pnumber = None
    div = None
    l_elements = {}

    for pnumber, prefix, verse, suffix in sorted_keys:
        if current_pnumber != pnumber:
            div = etree.SubElement(body, "div", attrib={"n": str(pnumber), "type": "psalm"})
            current_pnumber = pnumber

        l_key = (pnumber, prefix, verse, suffix)

        if l_key not in l_elements:
            lg = etree.SubElement(div, "lg", attrib={"n": f"{prefix}{verse}" if prefix else str(verse)})
            l_elements[l_key] = etree.SubElement(lg, "l", n=f"Ps {pnumber}:{prefix}{verse}{suffix}")
            etree.SubElement(l_elements[l_key], "app")

        l_element = l_elements[l_key]
        app = l_element.find("app")

        for file, lg_element in grouped_psalms[(pnumber, prefix, verse, suffix)]:
            tag = "lem" if file == "pivot.xml" else "rdg"
            element = etree.SubElement(app, tag)

            if tag == "rdg":
             element.set("wit", f"#{file.split('.')[0]}")
            
            for child in lg_element:
                element.append(deepcopy(child))

    return etree.tostring(new_root, pretty_print=True, encoding="utf-8", xml_declaration=True)


# Example usage
files = ["2FMP.xml", "Douce.xml", "2FMPV.xml", "pivot.xml"]
result_xml = process_xml_with_header(files)

with open("processed_output_with_header.xml", "wb") as f:
    f.write(result_xml)

print(result_xml.decode("utf-8"))

 <xsl:stylesheet 
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:tei="http://www.tei-c.org/ns/1.0"
        xmlns="http://www.tei-c.org/ns/1.0"
        exclude-result-prefixes="tei xs"
        version="2.0">
        
    <xsl:output method="xml" indent="yes"/>
             
        <!--　全コピーコードは順番が大事 -->
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
     
     
     <!-- Delete doubled pb ダブったquoteの削除にpb/cbが必要なので最後に処理する-->
     <xsl:template match="tei:pb">
         <xsl:choose>
             <xsl:when test="preceding::tei:pb[@n=current()/@n]">
             </xsl:when>
             <xsl:otherwise>
                 <xsl:copy-of select="."/>
             </xsl:otherwise>
         </xsl:choose>
     </xsl:template>
     
     <!-- Delete doubled cb -->
     <xsl:template match="tei:cb">
         <xsl:choose>
             <xsl:when test="preceding::tei:cb[@n=current()/@n]">
             </xsl:when>
             <xsl:otherwise>
                 <xsl:copy-of select="."/>
             </xsl:otherwise>
         </xsl:choose>
     </xsl:template>
     
     
     <!-- 他の要素は何も出力しない（デフォルト動作を無効化） -->
     <xsl:template match="//tei:lg/node()[not(self::tei:quote or self::tei:l or self::tei:pb or self::tei:cb or self::tei:note)]">
     </xsl:template>
     
     <!-- quote内の不要なlを削除 -->
     <!--<xsl:template match="//tei:quote/node()[not(self::tei:l)]"/>-->
     <xsl:template match="//tei:quote/tei:l"/>
     
     <!--Quote冒頭の|をquote内に入れ忘れた際に、lが二重生成されてしまう問題に念のため対応する-->
         <xsl:template match="tei:l[tei:l/tei:quote]"/>
    
    <!--fw内にｌを誤って生成した場合削除-->
    <xsl:template match="tei:fw/tei:l"/>


    <!-- Divに値を渡して用済みの@acroを削除-->
     <xsl:template match="@acro"/>
     
     <!--空のlを削除-->
     <xsl:template match="//tei:l[not(node())]"/>
 </xsl:stylesheet>


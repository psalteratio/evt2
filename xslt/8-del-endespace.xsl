 <xsl:stylesheet 
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:tei="http://www.tei-c.org/ns/1.0"
        xmlns="http://www.tei-c.org/ns/1.0"
        exclude-result-prefixes="tei xs"
        version="2.0">
        
    <xsl:output method="xml" indent="yes"/>
             
        <!--　全コピーコードは順番が大事 -->
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
     
   
    
     <!-- Match <l> elements and process their text nodes -->
     <xsl:template match="tei:l/text()">
         <xsl:value-of select="replace(., '\s+$', '')"/>
     </xsl:template>
     
    <!-- エラーチェック用！このテクストでは空になっているlgを削除（切り分け終わってないlgも消えるので注意）-->
     <xsl:template match="tei:lg[not(node())]"/>

     
 </xsl:stylesheet>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:xs="http://www.w3.org/2001/XMLSchema"
   xmlns:tei="http://www.tei-c.org/ns/1.0"
   xmlns="http://www.tei-c.org/ns/1.0"
   exclude-result-prefixes="tei xs"
   version="2.0">
   
   <xsl:output method="xml" indent="yes"/>
   
   
   <!-- Identity template to copy all nodes and attributes by default -->
   <xsl:template match="node()|@*">
      <xsl:copy>
         <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
   </xsl:template>
   
   <!-- Get Ps number in div as xml:id Divを作るなとの要請により封印-->
<!--   <xsl:template match="tei:lg">
      <!-\- 子ノードの最初のlのn属性をコピー -\->
      <lg>        
         <xsl:if test="tei:lg[1]/@n">
            <!-\-acrostic-\->
            <xsl:variable name="acro" select="@acro"/>  
            <!-\-<xsl:variable name="add-acro" select="concat('-', $acro)"/>  -\->
            <!-\- Ps番号だけを抽出してxml:id属性として持たせる -\->
            <xsl:analyze-string select="tei:lg[1]/@n" regex="Ps(\d+)-(\d+)([abcd])?">
               <xsl:matching-substring>
                  <xsl:variable name="Psn" select="regex-group(1)"/>  
                  <xsl:variable name="Psna" select="concat(regex-group(1),'-', $acro)"/>             
<!-\-      <xsl:choose>
         <xsl:when test="$acro">
            <xsl:attribute name="xml:id">
                          <xsl:value-of select="$Psna"/>
                       </xsl:attribute>
         </xsl:when>
         <xsl:otherwise>-\->
            <xsl:attribute name="n">
            <xsl:value-of select="$Psn"/>
                  </xsl:attribute>
         <!-\-</xsl:otherwise>
      </xsl:choose>-\->
               </xsl:matching-substring>
               
               <!-\- Ps番号が抽出できなかった場合、空のxml:idを作成 -\->
               <xsl:non-matching-substring>
                  <div> <xsl:attribute name="xml:id">
                     <xsl:value-of select="."/>
                  </xsl:attribute></div> 
               </xsl:non-matching-substring>
               
            </xsl:analyze-string>
         </xsl:if>
         <!-\- 子ノードの処理またはコピー -\->
         <xsl:apply-templates select="@* | node()"/>
      </lg>
   </xsl:template>-->
   
   <!--lg内のナンバー掃除　Div削除によりlgのnは必要なので復活-->
<!--   <xsl:template match="tei:lg/@n">
      <xsl:copy-of select="."/>
      <xsl:attribute name="n">
         <xsl:value-of select="substring-after(., '-')"/>
      </xsl:attribute>
   </xsl:template>  -->
   
   
   <!-- 指定要素以外を出力しない -->
   <xsl:template match="//tei:body/node()[not(self::tei:lg or self::tei:pb or self::tei:cb or self::tei:gap)]">
   </xsl:template>
   

   
<!--   <xsl:template match="//tei:lg[@type='strophe']">
      <xsl:copy-of select="."/>   
      <xsl:copy-of select="following::tei:head[1]"/>
      
   </xsl:template>
   -->
   
   <!-- 空のdivがあれば削除（タイトルコールなど、文字列があったがlに統合されていないものが消える -->
   <xsl:template match="tei:div[not(node())]"/>
   
  <!--text()を走査してgapを定義したnote回収-->
   <xsl:template match="tei:note/tei:note">
      <xsl:if test="contains(.,'&lt;gap')">
         <gap>
            <xsl:attribute name="extent">
               <xsl:value-of select="substring-before(substring-after(., 'extent=&#34;'),'&#34; reason')"/>
            </xsl:attribute>
            <xsl:attribute name="reason">
               <xsl:value-of select="substring-before(substring-after(., 'reason=&#34;'),'&#34;')"/>
            </xsl:attribute>
           
         </gap>
      </xsl:if>

  
   </xsl:template>
  
   
</xsl:stylesheet>
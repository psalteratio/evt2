 <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:tei="http://www.tei-c.org/ns/1.0"
        xmlns="http://www.tei-c.org/ns/1.0"
        exclude-result-prefixes="tei xs"
        version="2.0">
        
    <xsl:output method="xml" indent="yes"/>
           
     <!-- Identity template to copy all nodes and attributes by default -->
     <xsl:template match="node()|@*">
         <xsl:copy>
             <xsl:apply-templates select="@*|node()"/>
         </xsl:copy>
     </xsl:template>
           
           
     <!--Latin incipit yellow-->
     <xsl:template match="tei:hi[matches(@rend,'bg_yellow')]">
         <!--<head>-->
         <l>
             <quote xml:lang="latn">
             <xsl:apply-templates select="node()"/>
         </quote>
             </l>
             <!--</head>-->
     </xsl:template>
     
     <!--handShift-->
     <xsl:template match="tei:hi[matches(@rend,'bg_black')]">
         <handShift>
         <xsl:attribute name="scribe">
             <xsl:value-of select="substring-before(substring-after(.,'scribe=&#34;'),'&#34;')"/>
             </xsl:attribute>
         </handShift>
     </xsl:template>
     
     <!--Catchwords lg cyan-->
     <xsl:template match="tei:hi[matches(@rend,'bg_cyan')]">    
         <fw type="catch">
             <xsl:attribute name="place">
                 <xsl:value-of select="substring-before(substring-after(.,'place=&#34;'),'&#34;')"/>
             </xsl:attribute> 
             <xsl:apply-templates select="substring-before(.,'(')"/>
         </fw>
     </xsl:template>
     
     <!--Delete red-->
     <xsl:template match="tei:hi[matches(@rend,'bg_red')]">
         <del rend="dot">
             <xsl:apply-templates select="node()"/>
         </del>
     </xsl:template>
     
     <!--  <insert><end insert> green  -->
     <xsl:template match="tei:hi[matches(@rend,'bg_green')]">
         <add>
             <xsl:attribute name="place">
                 <xsl:value-of select="substring-before(substring-after(.,'place=&#34;'),'&#34;')"/>
             </xsl:attribute>
             <!--<xsl:apply-templates select="node()"/>-->
             <xsl:value-of select="substring-before(.,'(')"/>
         </add>
     </xsl:template>
     
     <!--  Blank space darkGrey  -->
     <xsl:template match="tei:hi[matches(@rend,'bg_darkGray')]">
         <xsl:analyze-string select="." regex="(&lt;)?[a-zA-Z]+?\s?[a-zA-Z]+?(\d+)(&gt;)?">
             <!--If the number is found-->
             <xsl:matching-substring>
                <space unit="chars">
             <xsl:attribute name="quantity">
                 <xsl:value-of select="regex-group(2)"/>
             </xsl:attribute>
                </space>
             </xsl:matching-substring>
             
             <!--If not found the number of spaces-->
         <xsl:non-matching-substring>
             <space unit="chars"/>
         </xsl:non-matching-substring>   
         </xsl:analyze-string>
     </xsl:template>
     
   <!--  Lettrine GREAT TEXT DIVISION-->
     <xsl:template match="tei:hi[@rend='b']">      
                 <g rend="lettrin">
                         <xsl:value-of select="."/>
                     </g>
     </xsl:template>
     
     <!--Abbreviation italic-->
     <xsl:template match="tei:hi">
         <abbr>
             <xsl:apply-templates select="node()"/> <!-- 子ノードを適切に処理 -->
         </abbr>
     </xsl:template>
     
     <!--Damage -->
     <xsl:template match="tei:hi[matches(@rend,'bg_darkYellow')]">
         <xsl:choose>
             <xsl:when test="substring-before(substring-after(.,'&lt;'),'&gt;')='STAIN' or 'stain'">
                 <damage agent="stain">
             <unclear>
             <xsl:text>[-]</xsl:text>
                 </unclear>
         </damage>
             </xsl:when>
             <xsl:otherwise>
                 <damage>
                 <xsl:attribute name="agent">
                     <xsl:value-of select="substring-before(substring-after(.,'&lt;'),'&gt;')"/>
                 </xsl:attribute>
                 </damage>
             </xsl:otherwise>
         </xsl:choose>
         
     </xsl:template>
 
     <!--Delete empty p-->     
     <xsl:template match="tei:p[not(node())]"/>
 
     <!-- Delete doubled pb -->
     <xsl:template match="tei:pb">
         <xsl:choose>
             <xsl:when test="preceding::tei:pb[@n=current()/@n]">
             </xsl:when>
             <xsl:otherwise>
                 <xsl:copy-of select="."/>
             </xsl:otherwise>
         </xsl:choose>
     </xsl:template>
    
    <!--gapをnoteから取り出す。ただのnoteはそのまま-->
    <xsl:template match="tei:note">
       <xsl:choose>
          <xsl:when test="tei:gap">
             <xsl:copy-of select="tei:gap"/>
          </xsl:when>
          <xsl:otherwise>
             <xsl:copy-of select="."/>
          </xsl:otherwise>
       </xsl:choose>
    </xsl:template>

      
       
     <xsl:template match="text()">
         <!-- 1つ目と2つ目の正規表現をまとめて処理 -->
         <xsl:analyze-string select="." regex="\s*(\+?)(\s| )?(Ps|Lk|Dn|Te Deum|Gloria|Pater|Credo|Addition|QVult)(\s| )(\d+):?([a-z]*\d+)?((bis|[a-z]*)?\-?[0-9]?)(\??)|(\s| )?(Ps|Lk|Dn|Te Deum|Gloria|Pater|Credo|Addition|QVult)(\s| )(\d+)?:?([A-Za-z])(\d+)">
             <!-- 最初の正規表現にマッチする部分 -->
             <xsl:matching-substring>
                 <xsl:variable name="org" select="regex-group(1)"/>
                 <xsl:choose>
                     <!-- 1つ目の正規表現のグループにマッチした場合 -->
                     <xsl:when test="regex-group(5)">
                         <xsl:variable name="chap" select="regex-group(3)"/>
                         <xsl:variable name="cn" select="regex-group(5)"/>
                         <xsl:variable name="ln" select="concat('-',regex-group(6))"/>
                         <xsl:variable name="partie" select="regex-group(7)"/>
                         <xsl:variable name="incert" select="regex-group(8)"/>
                         
                         <lg>
                             <xsl:attribute name="n" select="concat($chap, $cn, $ln, $partie)"/>
                             <xsl:if test="$org">
                                 <xsl:attribute name="corresp">#</xsl:attribute>
                             </xsl:if>
                         </lg>
                     </xsl:when>
                     <!-- 2つ目の正規表現のグループにマッチした場合 -->
                     <xsl:when test="regex-group(11)">
                         <xsl:variable name="chap" select="regex-group(11)"/>
                         <xsl:variable name="ln" select="concat('-',regex-group(13))"/>
                         <xsl:variable name="d" select="regex-group(14)"/>
                         <xsl:variable name="n" select="regex-group(15)"/>
                         
                         <lg>
                             <xsl:attribute name="n" select="concat($chap, $ln, $d, $n)"/>
                         </lg>
                     </xsl:when>
                 </xsl:choose>
             </xsl:matching-substring>
             <!-- 一致しない部分はそのまま出力 -->
             <xsl:non-matching-substring>
                 <xsl:value-of select="."/>
             </xsl:non-matching-substring>
         </xsl:analyze-string>     
      </xsl:template>
 
     
    

<!--Replace the tag "p" to "lg" with acrostic group Divいらねぇと言われたのでオフにするが必要になりそうな予感もする-->
    <xsl:template match="tei:p">
      <!-- <xsl:variable name="acrostic" select="matches(normalize-space(.), '^&lt;([A-Z0-9]+)\s?&gt;')" />
       <div type="psalm">
          <xsl:if test="$acrostic">
             <xsl:attribute name="acro">
                 <xsl:value-of select="replace(normalize-space(.), '^&lt;([A-Z0-9]+)\s?&gt;.*', '$1')" />
             </xsl:attribute>
          </xsl:if>-->
          <xsl:apply-templates select="node()" />
       <!--</div>-->
    </xsl:template>
     
     <!-- <lb/> を単に削除する処理 -->
     <xsl:template match="tei:lb"/>
     
     <!-- 生成された空の<lg/> を削除する処理 -->
     <xsl:template match="lg[not(node())]"/> 
     

     
     
 </xsl:stylesheet>


 <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:tei="http://www.tei-c.org/ns/1.0"
        xmlns="http://www.tei-c.org/ns/1.0"
        exclude-result-prefixes="tei xs"
        version="2.0">
        
    <xsl:output method="xml" indent="yes"/>
             
     
    <!-- Identity template to copy all nodes and attributes by default -->
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
   
     
     
     <!--lgタグをオープンタグにする 多分生成したてのlをキャッチしていない-->
     <xsl:template match="tei:lg">
         <lg type="verse">
             
             <!-- 自身の属性をコピー -->
             <xsl:copy-of select="@*"/>
             
     <!--        <lg type="strophe">-->
             
             
             <!-- 自身の内容をコピー -->
             <xsl:apply-templates select="node()"/>
             
             <!-- 次のlg要素の直前までのノードを含める -->
             <xsl:apply-templates select="following-sibling::node()[not(self::tei:lg)][generate-id(preceding-sibling::tei:lg[1]) = generate-id(current())]"/>
                 
             </lg>
     </xsl:template>
     
     
      <!--Add supplied-->
     <xsl:template match="text()">   
         <xsl:analyze-string select="." regex="\[([a-zA-Z]+)\]">
             <xsl:matching-substring>
                 <supplied>
                     <xsl:value-of select="regex-group(1)"/>
                 </supplied>
             </xsl:matching-substring>     
         <xsl:non-matching-substring>
             <xsl:value-of select="."/>
         </xsl:non-matching-substring>
         
         </xsl:analyze-string>
         
     </xsl:template>
     
     
 
 </xsl:stylesheet>


 <xsl:stylesheet 
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:tei="http://www.tei-c.org/ns/1.0"
        xmlns="http://www.tei-c.org/ns/1.0"
        exclude-result-prefixes="tei xs"
        version="2.0">
        
    <xsl:output method="xml" indent="yes"/>
             
        <!--　全コピーコードは順番が大事 -->
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
     
     <!--lb Generation in lg-->
     <xsl:template match="text()">
     <xsl:analyze-string select="." regex="\|">   
         <xsl:matching-substring>
             <lb/>
         </xsl:matching-substring>
         <xsl:non-matching-substring>
             <xsl:value-of select="."/>
         </xsl:non-matching-substring>
     </xsl:analyze-string>                
     </xsl:template>
     

</xsl:stylesheet>
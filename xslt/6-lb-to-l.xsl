 <xsl:stylesheet 
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:tei="http://www.tei-c.org/ns/1.0"
        xmlns="http://www.tei-c.org/ns/1.0"
        exclude-result-prefixes="tei xs"
        version="2.0">
        
    <xsl:output method="xml" indent="yes"/>
             
        <!--　全コピーコードは順番が大事 -->
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
     
   <xsl:template match="tei:lb">
       <!-- 自身の属性をコピー -->
       <l>  
       
<!--       <!-\- 自身の内容をコピー -\->
       <xsl:apply-templates select="node()"/>-->
       
       <!-- 次のlg要素の直前までのノードを含める -->
       <xsl:apply-templates select="following-sibling::node()[not(self::tei:lb)][generate-id(preceding-sibling::tei:lb[1]) = generate-id(current())]"/>
       
       <!--</lg>-->
       </l>
       
   </xsl:template>

<xsl:template match="l">
    <xsl:if test="following-sibling::l[1]=current()"></xsl:if>
</xsl:template>
     


 </xsl:stylesheet>


 <xsl:stylesheet 
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:tei="http://www.tei-c.org/ns/1.0"
        xmlns="http://www.tei-c.org/ns/1.0"
        exclude-result-prefixes="tei xs"
        version="2.0">
        
    <xsl:output method="xml" indent="yes"/>
             
        <!--　全コピーコードは順番が大事 -->
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
     
     <!--Generation of pb,rv,cb-->
     <xsl:template match="text()">
         <xsl:analyze-string select="." regex="&lt;f.\s?(\d+|I*)([rv])([abcd])?&gt;">   
         <xsl:matching-substring>
             <xsl:variable name="pb" select="regex-group(1)"/>
             <xsl:variable name="rv" select="regex-group(2)"/>
             <xsl:variable name="cb" select="regex-group(3)"/>
             
              <pb>
                 <xsl:attribute name="n" select="concat($pb, $rv)"/></pb>
             
            <!-- もしcbがあるテクストの場合はcbを生成-->
             <xsl:if test="regex-group(3)">
             <cb>
                 <xsl:attribute name="n" select="concat($pb,$rv,$cb)"/></cb>
             </xsl:if>
         </xsl:matching-substring>
         <xsl:non-matching-substring>
             <xsl:value-of select="."/>
         </xsl:non-matching-substring>
     </xsl:analyze-string>                
     </xsl:template>

    <!--text()を走査してgapを定義したnote回収　note/noteでないと動かないのが問題 gapに関するエンコード方針はTeiheaderに入れるらしい-->
    <xsl:template match="tei:note/tei:note">
       <xsl:if test="contains(.,'&lt;gap')">
          <gap>
             <xsl:attribute name="extent">
                <xsl:value-of select="substring-before(substring-after(., 'extent=&#34;'),'&#34; reason')"/>
             </xsl:attribute>
             <xsl:attribute name="reason">
                <xsl:value-of select="substring-before(substring-after(., 'reason=&#34;'),'&#34;')"/>
             </xsl:attribute>
             
          </gap>
       </xsl:if>
    </xsl:template>
     

 </xsl:stylesheet>

